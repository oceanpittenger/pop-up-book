/**
 *
 */

var app;
(function ($, Modernizr) {

    var Building = function(data)
    {
        this.path = data.path;
        this.name = data.name;
        this.description = data.description;
        this.themeColor = data.themeColor;
        this.themeColorMobile = data.themeColorMobile;
        this.closeColor = data.closeColor;
        this.learnMoreColor = data.learnMoreColor;
        this.learnMoreText = data.learnMoreText;
        //this.clickIcon = data.clickIcon;
        this.images = data.images;
        this.rooms = new Array();
        for(var i in data.rooms)
        {
            var r = data.rooms[i];
            var room = new Room(r, this);
            this.rooms.push(room);
        }
    };
    Building.prototype.getPath = function()
    {
        return this.path;
    };

    Building.prototype.draw = function() //roof
    {
        var source = $("#building-template").html();
        var template = Handlebars.compile(source);
        var context = {
            name: this.name,
            description: this.description,
            images: this.images,
            path: this.path,
            themeColor: this.themeColor,
            themeColorMobile: this.themeColorMobile,
            closeColor: this.closeColor,
            learnMoreColor: this.learnMoreColor,
            learnMoreText: this.learnMoreText,
            firstRoom: app.content.firstRoom(),
            clickIcon: this.clickIcon


        };
        var html = template(context);

        return html;
    };
    Building.prototype.drawBottom = function()
    {
        var source = $("#building-bottom-template").html();
        var template = Handlebars.compile(source);

        var previousBuilding = app.content.previousBuilding();
        var nextBuilding = app.content.nextBuilding();

        var context = {
            name: this.name,
            description: this.description,
            images: this.images,
            path: this.path,
            themeColor: this.themeColor,
            themeColorMobile: this.themeColorMobile,
            closeColor: this.closeColor,
            learnMoreColor: this.learnMoreColor,
            learnMoreText: this.learnMoreText, 
            lastRoom: app.content.lastRoom(),
            nextBuilding: (nextBuilding).getPath(),
            nextBuildingColor: (nextBuilding).getThemeColor(),
            previousBuildingColor: (previousBuilding).getThemeColor(),
            previousBuilding: (previousBuilding).getPath(),
            nextBuildingIcon: nextBuilding.getImage("nextBuildingIcon"),
            previousBuildingIcon: previousBuilding.getImage("prevBuildingIcon"),
            comic: this.getImage("comic"),
            comicImg: this.getImage("comicImg"),
            closeX: this.getImage("closeX"),
            //nextPrevIcon: this.getImage("nextPrevIcon"),
            nextBuildingName: nextBuilding.getName(),
            previousBuildingName: previousBuilding.getName()


        };
        var html = template(context);

        return html;
    };

    Building.prototype.drawForHome = function()
    {
        var source = $("#building-menu-template").html();
        var template = Handlebars.compile(source);
        var context = {
            name: this.name,
            images: this.images,
            path: this.path,
            themeColor: this.themeColor,
            themeColorMobile: this.themeColorMobile,
            closeColor: this.closeColor,
            learnMoreColor: this.learnMoreColor,
            learnMoreText: this.learnMoreText,
            clickIcon: this.clickIcon
        };
        var html = template(context);

        return html;
    };
    Building.prototype.getImage = function(imageKey)
    {
        return this.images[imageKey];
    };
    Building.prototype.getRoomCount = function()
    {
        return this.rooms.length;
    }
    Building.prototype.getRooms = function()
    {
        return this.rooms;
    };
    Building.prototype.getRoom = function(roomId)
    {
        if(roomId >= this.rooms.length)
        {
            return null; // this should not happen
        }
        return this.rooms[roomId];
    }
    Building.prototype.getName = function()
    {
        return this.name;
    }
    Building.prototype.getThemeColor = function()
    {
        return this.themeColor;
    }
    Building.prototype.getThemeColorMobile = function()
    {
        return this.themeColorMobile;
    }
    Building.prototype.getCloseColor = function()
    {
        return this.closeColor;
    }
    Building.prototype.getLearnMoreColor = function()
    {
        return this.learnMoreColor;
    }
    Building.prototype.getLearnMoreText = function()
    {
        return this.learnMoreText;
    }
    Building.prototype.getClickIcon = function()
    {
        return this.clickIcon;
    }


    var Room = function(data, parent)
    {
        this.title = data.title;
        this.subtitle = data.subtitle;
        this.description = data.description;
        this.images = data.images;
        this.parent = parent;
        this.facebookLink = data.facebookLink;
        this.twitterLink = data.twitterLink;

        this.clickPoints = new Array();
        for(var i in data.clickpoints)
        {
            var c = data.clickpoints[i];
            var cp = new ClickPoint(c);
            this.clickPoints.push(cp);
        }
    };
    Room.prototype.draw = function(id)
    {
        var clickpointsHtml = "";
        for(var k in this.clickPoints)
        {
            clickpointsHtml += this.clickPoints[k].draw(id+"-"+k, this.parent.getImage("icon"),  this.parent.getThemeColor(), this.parent.getCloseColor(), this.parent.getImage("closeX"), this.parent.getLearnMoreColor(), this.parent.getLearnMoreText(), this.parent.getClickIcon());
        }

        var prevRoom = app.content.previousRoom();
        if(prevRoom !== null)
        {
            prevRoom = prevRoom.toString();
        }

        var nextRoom = app.content.nextRoom();
        if(nextRoom !== null)
        {
            nextRoom = nextRoom.toString();
        }


        var source = $("#room-template").html();
        var template = Handlebars.compile(source);
        var context = {
            id: id,
            title: this.title,
            subtitle: this.subtitle,
            description: this.description, 
            clickpoints: clickpointsHtml,
            images: this.images,
            themeColor: this.parent.getThemeColor(),
            themeColorMobile: this.parent.getThemeColorMobile(),
            closeColor: this.parent.getCloseColor(),
            learnMoreColor: this.parent.getLearnMoreColor(),
            learnMoreText: this.parent.getLearnMoreText(),
            parentPath: this.parent.getPath(),
            previousRoom: prevRoom,
            nextRoom: nextRoom,
            closeX: this.parent.getImage('closeX'),
            facebookLink: this.facebookLink,
            twitterLink: this.twitterLink,
            url: this.url

        };
        var html = template(context);

        return html;
    };
    Room.prototype.getImage = function(key)
    {
        return this.images[key];
    };
    Room.prototype.getClickPointCount = function()
    {
        return this.clickPoints.length;
    };




    var ClickPoint = function(data)
    {
        this.x              = data.x;
        this.y              = data.y;
        this.content        = data.content;
        this.url            = data.url;
        this.themeColor     = data.themeColor;
        this.closeColor     = data.closeColor;
        this.learnMoreColor = data.learnMoreColor;
        this.learnMoreText  = data.learnMoreText;
        this.clickIcon      = data.clickIcon;
        this.learnMoreClass = data.learnMoreClass;
    };
    ClickPoint.prototype.draw = function(id, icon, themeColor, closeColor, closeX, learnMoreColor, learnMoreText)
    {
        var source = $("#clickpoint-template").html();
        var template = Handlebars.compile(source);
        var context = {
            id: id,
            x: this.x,
            y: this.y,
            url: this.url,
            content: this.content,
            icon: icon,
            themeColor: themeColor,
            closeColor: closeColor,
            closeX: closeX,
            learnMoreColor: learnMoreColor,
            learnMoreText: learnMoreText,
            clickIcon: this.clickIcon,
            learnMoreClass: this.learnMoreClass


        };
        var html = template(context);

        return html;
    };



    /**
     * The main app
     * contains functions that are targets for routing, navigational utilities, setup, etc
     *
     */
    app = {

        buildings: new Array(),



        init: function()
        {

            app.setupBuildings();
            app.content.showMenu();

            // if we're on the home page, draw everything
            if(app.navigation.getCurrentPage() == app.navigation.HOME_PAGE ||
                app.navigation.getCurrentPage() == app.navigation.HOME_PAGE_NO_FILE)
            {
                app.content.showHomePage();
            }
        },

        goToBuilding: function(industryName)
        {
            // redirect if needed
            app.navigation.redirectWithHash(app.navigation.BUILDING_PAGE+"#/building/"+industryName);
            console.log(industryName);

            app.content.loadBuilding(industryName);
            app.content.showBuilding();
        },

        goToBuildingBottom: function(industryName)
        {
            // redirect if needed
            app.navigation.redirectWithHash(app.navigation.BUILDING_PAGE+"#/building/"+industryName+"/bottom");
            console.log(industryName);

            app.content.loadBuilding(industryName);
            app.content.showBuildingBottom();
        },

        goToRoom: function(industryName, roomId)
        {
            // redirect if needed - skip first room to prevent double scrolling to room.
            if (app.content.currentBuildingId == null) {
                app.navigation.redirectWithHash(app.navigation.BUILDING_PAGE+"#/building/"+industryName+"/"+roomId);
            }

            app.content.loadBuilding(industryName);
            app.content.showRoom(roomId);
        },

        goHome: function()
        {
            // redirect if needed
            app.navigation.redirectWithHash(app.navigation.HOME_PAGE);

            app.content.showHomePage();
        },

        setupBuildings: function()
        {
            //console.log("in setup buildings");
            for(var k in content.industries)
            {
                var b = new Building(content.industries[k]);
                app.buildings.push(b);
            }
        },

        openClickpointContent: function(id)
        {
            $("#"+id).show();
            
            $('#content-container .room-wrapper .mobile-clickpoints #' +id).show();

            console.log("Open me: #" + id );
        },

        closeClickpointContent: function(id)
        {
            $("#"+id).hide();
            $('#content-container .room-wrapper .mobile-clickpoints #' +id).hide();
        },

        openInfoContent: function()
        {
            $('.info-content').show();
            
        },

        closeInfoContent: function()
        {
            $('.info-content').hide();
        },

        openComic: function()
        {
            console.log('opening comic');
            $('.comicModal').show();

        },

        closeComic: function()
        {
            $('.comicModal').hide();
            console.log('close comic');
        },



        /**
         * The navigation object has helper functions
         * to make sure that we get to the right url paths
         */
        navigation: {

            HOME_PAGE: "index.html",
            HOME_PAGE_NO_FILE: "", // this is empty string
            BUILDING_PAGE: "building.html",


            /**
             * @returns {string} The current html page
             */
            getCurrentPage: function()
            {
                var path = window.location.pathname;
                path = path.substring(path.lastIndexOf("/")+1);
                //console.log(path);

                return path;
            },

            /**
             * Ensures that we are on the correct page,
             * if not, we redirect to it, maintaining the current hash path
             */
            redirectWithHash: function(redirectPath)
            {
                var path = app.navigation.getCurrentPage();
                var redirectPage = redirectPath.substring(redirectPath.lastIndexOf("/")+1);
                if(path != redirectPage)
                {
                    window.location.href = redirectPath;
                }
            }

        },

        content: {

            currentBuildingId: null,
            currentRoomId: null,
            bottom: false,

            /**
             * Loads the building with the given name
             * Does not actually display the building
             * @param industryName
             */
            loadBuilding: function(industryName)
            {
                //console.log(industryName);
                var newBuildingId = null;
                for(var i in app.buildings)
                {
                    //console.log(app.buildings[i].getPath());
                    if(industryName == app.buildings[i].getPath())
                    {
                        newBuildingId = i;
                    }
                }

                if(newBuildingId == app.content.currentBuildingId)
                {
                    // we are already on this building, do nothing
                    //console.log("already here " + newBuildingId);
                    return;
                }

                //console.log("loading building: " + newBuildingId);

                app.content.currentBuildingId = newBuildingId;

                app.content.bottom = false;

            },

            /**
             * Draws the building top for the currently loaded building
             */
            showBuilding: function()
            {
                // load the building assets and stuff onto the page
                app.content.currentRoomId = null;
                var b = app.buildings[app.content.currentBuildingId];
                var html = b.draw();

                $("#content-container").html(html);

                //console.log("loaded top of building");


                $("#content-container .building-wrapper").delay(1000).animate({
                        'top': "-800px"
                    },
                    1000,
                    'swing',
                    function(){
                        // animation complete
                        // go to first room
                        var bb = app.buildings[app.content.currentBuildingId];
                        app.goToRoom( bb.path, 0);
                        //$('a.up-arrow').css({'display': 'none'});
                    }
                );

            },

            showBuildingBottom: function()
            {
                app.content.currentRoomId = null;
                var b = app.buildings[app.content.currentBuildingId];
                //var r = b.getRoom(roomId);

                var html = b.drawBottom();


                $('#content-container').html(html);

                app.content.bottom = true;


                $("#content-container .bottom-city").animate(
                        {
                            'bottom':'0'
                        },
                        1000,
                        'swing',
                        function(){
                            // animation end
                        }
                    );

                



                //app.content.bottom = true;
                $('.mobile-comic-btn').on('click', app.openComic);
                $('.mobile-close').on('click', app.closeComic);
                $('.comic-btn').on('click', app.openComic);
                $('.close').on('click', app.closeComic);
            },

            /**
             * Draws a room in the currently loaded building
             * @param roomId
             */
            showRoom: function(roomId)
            {

                var b = app.buildings[app.content.currentBuildingId];
                var r = b.getRoom(roomId);

                
                /*if(app.content.currentRoomId == roomId)
                {
                    // already there!
                    return;
                }*/

                if(app.content.currentRoomId == r)
                {
                    // already there!
                    return;
                }

                // the default for "directionUp" is true,
                // unless we're going to a room with an ID "less than" our current room
                var directionUp = true;
                var animDelay = 0;
                if(app.content.currentRoomId !== null  || app.content.bottom == true)
                {
                    if(app.content.currentRoomId > roomId || app.content.bottom == true)
                    {
                        directionUp = false;
                    }

                    // is the current room there?
                    // if so, remove it
                    app.content.removeCurrentRoom(directionUp);
                    animDelay = 1000;
                }

                // set the new current room to this room id
                app.content.currentRoomId = parseInt(roomId);

                var b = app.buildings[app.content.currentBuildingId];
                var r = b.getRoom(roomId);
                var html = r.draw();

                $("#content-container").delay(animDelay).queue( function(next){
                    $(this).html(html);

                    if(!directionUp)
                    {
                        $("#content-container .room-wrapper").css("top", "-1000px");
                    }

                    $("#content-container .room-wrapper").animate(
                        {
                            'top':'0px'
                        },
                        1000,
                        'swing',
                        function(){
                            // animation end
                        }
                    );



                    next();

                });

                app.content.bottom = false;

            },

            /**
             * Causes the current room to slide away
             */
            removeCurrentRoom: function(directionUp)
            {
                var goalTop = "-1000px";
                if(!directionUp)
                {
                    goalTop = "1000px";
                }
                console.log("removing room: " + goalTop);
                $('#content-container .room-wrapper').animate(
                    {
                        'top': goalTop
                    },
                    1000,
                    'swing'
                );
            },

            showHomePage: function()
            {
                var buildingHtml = "";
                for(var k in app.buildings)
                {
                    var b = app.buildings[k];
                    buildingHtml += b.drawForHome();
                }

                $("#buildings-container").html(buildingHtml);

            },

            showMenu: function()
            {
                var menuItems = new Array();
                for(var k in app.buildings)
                {
                    var b = app.buildings[k];
                    var rooms = b.getRooms();
                    var subMenuItems = new Array();
                    for(var j in rooms)
                    {
                        var r = rooms[j];
                        var item = {
                            title: r.title,
                            path: "#/building/" + b.getPath() + "/" + j
                        };
                        subMenuItems.push(item);
                    }

                    var item = {
                        title: b.name,
                        path: "#/building/" + b.getPath(),
                        subMenuItems: subMenuItems
                    };
                    menuItems.push(item);
                }

                //console.log(menuItems);

                var source = $("#menu-template").html();
                var template = Handlebars.compile(source);
                var context = {
                    menuItems: menuItems
                };
                var html = template(context);

                $("#menu-container").html(html);
                $("#mobile-menu-container").html(html);

               /* $('.close-button').click (function(){
                    //$('#mobile-menu-container').hide();
                    $('#mobile-menu-container ul.menu-nav').css({'display': 'none'});
                    $('#mobile-menu-container').html("All Industries" + html);
                    console.log('testing close');
                });*/

                // if we're on tablet, use a class to show/hide the menu
                if (Modernizr.touchevents && Modernizr.mq('(min-width: 767px)')) {
                    $('#menu-container').on('touchstart', function(e){
                        $('.hide-menu').removeClass('hide-menu');
                        $(this).addClass('active');
                    });

                    $('#menu-container.active a').on('touchstart', function(e){
                        $('.hide-menu').removeClass('hide-menu');
                        $('#menu-container').removeClass('active');
                    });

                    $('.clouds').on('touchstart', function(e){
                        $('#menu-container').removeClass('active');
                    });

                    $('.sub-menu-title').click(function() {
                        $('.sub-menu-list').removeClass('hidden').addClass('hide-menu');
                    });
                }
            },

            nextBuilding: function()
            {
                var currentId = app.content.currentBuildingId;
                currentId++;
                if(currentId >= app.buildings.length)
                {
                    currentId = 0; // cycle back to beginning
                }

                return app.buildings[currentId];
            },

            previousBuilding: function()
            {
                var currentId = app.content.currentBuildingId;
                currentId--;
                if(currentId < 0)
                {
                    currentId = app.buildings.length - 1; // cycle back to end
                }

                return app.buildings[currentId];
            },

            firstRoom: function()
            {
                return 0;
            },

            lastRoom: function()
            {
                var b = app.buildings[app.content.currentBuildingId];
                var total = b.getRoomCount();
                return total - 1;
            },

            nextRoom: function()
            {
                var b = app.buildings[app.content.currentBuildingId];
                var total = b.getRoomCount();

                //console.log("total room count: " + total);
                console.log("current room id: " + app.content.currentRoomId);

                if(app.content.currentRoomId >= total - 1)
                {
                    return null;
                }

                return app.content.currentRoomId + 1;
            },

            previousRoom: function()
            {
                if(app.content.currentRoomId > 0)
                {
                    return app.content.currentRoomId - 1;
                }
                return null;
            }

        }

    };


    /**
     * Routes
     */


    var routes = {
        '/': app.goHome,
        '/building': app.goHome, // if industry is not specified
        '/building/:industryName': app.goToBuilding,
        '/building/:industryName/bottom': app.goToBuildingBottom,
        '/building/:industryName/:roomId': app.goToRoom
    };

    var router = Router(routes);


    $(document).ready(function() {

        app.init();
        router.init();

        $('.comicModal').click(function(event) {
            event.preventDefault();
            $('.comicModal').fadeOut(400);
        });


        $('.mobile-menu-button').click(function(e){
            e.preventDefault();
            $('#mobile-menu-container ul.menu-nav').show();
            $('#mobile-menu-container').removeClass('not-expanded').addClass('expanded');
            $('.mobile-menu-button').hide();

        });

        $('.menu-nav').click(function(e){
            $('#mobile-menu-container').removeClass('expanded');
            $('.mobile-menu-button').show();
            $('#menu-container ul.menu-nav .sub-menu-list').show();

        });

    });

})(jQuery, window.Modernizr);
