/**
 *
 */

var content = {

    industries: [

        {
            path: "retail",
            name: "Retail",
            description: "",
            themeColor:"rgba(240, 78, 55, 1)",
            themeColorMobile:"rgba(240, 78, 55, 0.7)",
            closeColor:"#ffffff",
            learnMoreColor: "#fdb813",
            learnMoreText: "#000000",

            images: {
                icon: "retail/icon.png",
                nextBuildingIcon: "retail/nextBuildingIcon.png",
                prevBuildingIcon: "retail/prevBuildingIcon.png",
                building: "retail/retail_building.png",
                roof: "retail/Retail_Roof.png",
                base: "retail/base.png",
                comic: "retail/ComicBookButton.png",
                comicImg: "retail/comicImg.jpg",
                prevIcon: "financial/financial.png",
                nextIcon: "healthcare/healthcare.png",
                closeX: "retail/CloseWhite.png"
            },

            rooms: [
                {
                    title: "Retail Industry",
                    subtitle: "Storage Challenges",
                    description: "In retail, speedy results lead to happier customers. IBM FlashSystems offers solutions that keep business costs down and meet high customer expectations.",
                    facebookText:"",
                    twitterText:"",
                    images: {
                        room: "retail/Retail_Room_00.png"
                    },

                    clickpoints: [
                        {
                            x: 50,
                            y: 220,
                            content: "<h2 class='retail-header'>Challenge:</h2><p class='retail-content'>Giving customers the quick responses they demand</p><h2 class='retail-header'>Solution:</h2><p class='retail-content-small'>As Flash dramatically reduces the latency of storage, the ultra-fast response times serves data quickly – giving customers a superior experience</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon: "modal_icons_blank.png"
                        },
                        {
                            x: 200,
                            y: 250,
                            content: "<h2 class='retail-header'>Challenge:</h2><p class='retail-content'>Keeping costs down</p><h2 class='retail-header'>Solution:</h2><p class='retail-content-small'>IBM Flash takes up minimal space –  meaning low energy consumption and costs </p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        },
                        {
                            x: 300,
                            y: 296,
                            content: "<h2 class='retail-header'>Challenge:</h2><p class='retail-content'>Keeping costs down</p><h2 class='retail-header'>Solution:</h2><p class='retail-content-small'>The capacity and capabilities of Flash create faster response times even on heavy shopping days.</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        }
                    ]
                },
                {
                    title: "Clarks",
                    subtitle: "",
                    description: "Clarks' online business depended on knowing the shoes customers want, and delivering them quickly. They needed the speedy analytics to understand their growing customer base. IBM FlashSystem delivered the quick info with reduced procesing speeds.",
                    facebookText:"With IBM Flash Storage, Clarks gets crucial information in half the time – ensuring more on-time online delivery.",
                    twitterText:"",
                    images: {
                        room: "retail/Retail_Room_01.png"
                    },

                    clickpoints: [
                        {
                            x: 92,
                            y: 207,
                            content: "<h2 class='retail-header'>Maximized Performance</h2><p class='retail-content'>Spend more time on the important things when batch processing times are cut in half.</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03346GBEN&attachment=TSC03346GBEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_01.png"
                        },
                        {
                            x: 207,
                            y: 207,
                            content: "<h2 class='retail-header'>Additional Support</h2><p class='retail-content'>Faster processing speeds to support 100s of online shoppers</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03346GBEN&attachment=TSC03346GBEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_14.png"
                        },
                        {
                            x: 319,
                            y: 284,
                            content: "<h2 class='retail-header'>Up-to-date Info</h2><p class='retail-content'>Having the latest information leads to increased on-time order delivery</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03346GBEN&attachment=TSC03346GBEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_18.png"
                        }
                    ]
                },
                {
                    title: "Hansa-Flex",
                    subtitle: "",
                    description: "When a hydraulic system needs replacement, there's no time to lose. Hydraulic specialist, HANSA-FLEX, needed a solution that would help htem keep up with the pace of their rapidly growing business. IBM FlashSystem pushed them into a competitive position.",
                    facebookText:"When you have 1,400 users, you can’t afford downtime. With IBM Flash Storage, Hansa-Flex can run mission-critical SAP applications uninterrupted for better customer satisfaction.",
                    twitterText:"1,400 uses can’t afford downtime. Hansa-Flex runs their apps uninterrupted with #IBM #Flash",
                    images: {
                        room: "retail/Retail_Room_02.png"
                    },

                    clickpoints: [
                        {
                            x: 35,
                            y: 234,
                            content: "<h2 class='retail-header'>Round The Clock Service</h2><p class='retail-content'>Mission-critical SAP applications run uninterrupted for 1,400 users</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=SPC03533WWEN&attachment=SPC03533WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_16.png"
                        },
                        {
                            x: 131,
                            y: 285,
                            content: "<h2 class='retail-header'>Faster Process</h2><p class='retail-content'>56% better response times for better productivity</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=SPC03533WWEN&attachment=SPC03533WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_10.png"
                        },
                        {
                            x: 333,
                            y: 300,
                            content: "<h2 class='retail-header'>Flexibility To Grow</h2><p class='retail-content'>A scalable environment with minimized disruption leads to improved customer service</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=SPC03533WWEN&attachment=SPC03533WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_17.png"
                        }
                    ]
                },
                {
                    title: "PriceMinister.com",
                    subtitle: "",
                    description: "The leading French online marketplace, PriceMinister.com, needed to keep its market-leading position as its customer base grew. IBM FlashSystems helped them achieve a high-performance system for a  secure and seamless shopping experience.",
                    facebookText:"By simply implementing IBM Flash Storage, PriceMinister.com saw 180x faster business intelligence reporting.",
                    twitterText:"PriceMinister business intelligence reporting goes from 2 hours to 40 seconds with #IBM #Flash",
                    images: {
                        room: "retail/Retail_Room_03.png"
                    },

                    clickpoints: [
                        {
                            x: 90,
                            y: 213,
                            content: "<h2 class='retail-header'>180X Faster Reporting</h2><p class='retail-content'>Business Intelligence reporting went from 2 hours to 40 seconds with no need to restructure data.</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_WWEN&htmlfid=TSC03323WWEN&attachment=TSC03323WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_04.png"
                        },
                        {
                            x: 200,
                            y: 244,
                            content: "<h2 class='retail-header'>Spend Less With No Disks</h2><p class='retail-content'>Lower energy consumption makes for a highly cost-effective solution</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_WWEN&htmlfid=TSC03323WWEN&attachment=TSC03323WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_12.png"
                        },
                        {
                            x: 265,
                            y: 316,
                            content: "<h2 class='retail-header'>More Time For CX</h2><p class='retail-content'>With an easily managed storage system, teams can spend less time on admin and more time on the user experience</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_WWEN&htmlfid=TSC03323WWEN&attachment=TSC03323WWEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_06.png"
                        }
                    ]
                },
                {
                    title: "Coca-Cola Bottling Co. Consolidated",
                    subtitle: "",
                    description: "CCBCC needed to process more data in less time to take advantage of more sales opportunitites. With IBM FlashSystem, they could process 20x more data in the same overnight window.",
                    facebookText:"Using IBM Flash Storage, the Coca-Cola Bottling Co. gets information to their 47 distribution centers 4x faster.",
                    twitterText:"Faster info for 47 #CocaCola distribution centers all with #IBM #Flash",
                    images: {
                        room: "retail/Retail_Room_04.png"
                    },

                    clickpoints: [
                        {
                            x: 10,
                            y: 205,
                            content: "<h2 class='retail-header'>Improved Scheduling</h2><p class='retail-content'>Higher volumes of analysis means more on-time deliveries and overall improved profitability.</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_USEN&htmlfid=TSC03243USEN&attachment=TSC03243USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_08.png"
                        },
                        {
                            x: 152,
                            y: 268,
                            content: "<h2 class='retail-header'>Critical Info Fast</h2><p class='retail-content'>The 47 distribution centers can get inventory information 4 times faster with better back-end processing.</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_USEN&htmlfid=TSC03243USEN&attachment=TSC03243USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_07.png"
                        },
                        {
                            x: 300,
                            y: 253,
                            content: "<h2 class='retail-header'>Same Window, More Analysis</h2><p class='retail-content'>With batch processing times in 6-7 hours instead of 10-14, means getting the right product to the right shelf at the right time.</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_USEN&htmlfid=TSC03243USEN&attachment=TSC03243USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_03.png"
                        }
                    ]
                }
            ]
        },



        {
            path: "healthcare",
            name: "Healthcare",
            description: "",
            themeColor:"rgba(0, 166, 160, 1)",
            themeColorMobile:"rgba(0, 166, 160, 0.7)",
            closeColor:"#ffffff",
            learnMoreColor: "#7f1c7d",
            learnMoreText: "#ffffff",

            images: {
                icon: "healthcare/icon.png",
                nextBuildingIcon: "healthcare/nextBuildingIcon.png",
                prevBuildingIcon: "healthcare/prevBuildingIcon.png",
                building: "healthcare/healthcare_building.png",
                roof: "healthcare/roof.png",
                base: "healthcare/base.png",
                comic: "healthcare/ComicBookButton.png",
                comicImg: "healthcare/comicImg.jpg",
                prevIcon: "retail/retail.png",
                nextIcon: "financial/financial.png",
                closeX: "healthcare/CloseWhite.png"
            },

            rooms: [
                {
                    title: "Healthcare Industry",
                    subtitle: "Storage Challenges",
                    description: "In the healthcare industry, seconds equal lives. IBM FlashSystems provides faster access to crucial information while handling diverse workloads behind the scenes.",
                    facebookText:"",
                    twitterText:"",
                    images: {
                        room: "healthcare/Hospital_Room_00.png"
                    },

                    clickpoints: [
                        {
                            x: 80,
                            y: 280,
                            content: "<h2 class='healthcare-header'>Challenge:</h2><p class='healthcare-content'>Escalating EMR costs & flat budgets</p><h2 class='healthcare-header'>Solution:</h2><p class='healthcare-content-small'>Ensuring economics Flash is less than $2 per GB</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        },
                        {
                            x: 225,
                            y: 318,
                            content: "<h2 class='healthcare-header'>Challenge:</h2><p class='healthcare-content'>Need faster access to growing patient information</p><h2 class='healthcare-header'>Solution:</h2><p class='healthcare-content-small'>Scalable performance Flash handles diverse workloads\nRapid data growth</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        },
                        {
                            x: 307,
                            y: 269,
                            content: "<h2 class='healthcare-header'>Challenge:</h2><p class='healthcare-content'>Process accurate, secure reports in less time</p><h2 class='healthcare-header'>Solution:</h2><p class='healthcare-content-small'>Agile integration\nRuns faster analytics for improved reporting</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        }
                    ]
                },
                {
                    title: "Memorial Hermann",
                    subtitle: "",
                    description: "The doctors at Memorial Hermann need fast access to patient records in order to make insightful decisions. IBM FlashSystems made this happen while saving energy and valuable floor space.",
                    facebookText:"When response times were reduced by 99% with IBM Flash Storage, Memorial Hermann gained faster access to crucial patient records.",
                    twitterText:"99% faster response time when it matters most. #IBM #Flash #MemorialHermann",
                    images: {
                        room: "healthcare/Hospital_Room_01.png"
                    },

                    clickpoints: [
                        {
                            x: 50,
                            y: 247,
                            content: "<h2 class='healthcare-header'>Move Quickly</h2><p class='healthcare-content'>Faster access to patient records: Response time reduced by 99%</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_USEN&htmlfid=TSC03294USEN&attachment=TSC03294USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_05.png"
                        },
                        {
                            x: 175,
                            y: 262,
                            content: "<h2 class='healthcare-header'>Smaller Footprint</h2><p class='healthcare-content'>Physical space savings: 98% reduction in rack space</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_TS_ZU_USEN&htmlfid=TSC03294USEN&attachment=TSC03294USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_04.png"
                        },
                        {
                            x: 300,
                            y: 311,
                            content: "<h2 class='healthcare-header'>Energy Savings</h2><p class='healthcare-content'>Power and cooling requirements reduced by 95%</p>",
                            url: "https://www.youtube.com/watch?v=tepGSSIlA5M",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_11.png"
                        }
                    ]
                },
                {
                    title: "  FlashSystem with Epic ",
                    subtitle: "",
                    description: "Medical application maker, Epic, faces high usage, random storage access and stringent latency requirements. IBM FlashSystems helped ease these pain points with an increase in performance.",
                    facebookText:"Epic needed reliability, data protection and superior storage management. They found it with IBM Flash Storage.",
                    twitterText:"",
                    images: {
                        room: "healthcare/Hospital_Room_02.png"
                    },

                    clickpoints: [
                        {
                            x: 11,
                            y: 228,
                            content: "<h2 class='healthcare-header'>Dependable and Secure</h2><p class='healthcare-content'>Gain reliability, data protection and storage management</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=WH&infotype=SA&appname=STGE_TS_ZU_USEN&htmlfid=TSW03287USEN&attachment=TSW03287USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_13.png"
                        },
                        {
                            x: 129,
                            y: 291,
                            content: "<h2 class='healthcare-header'>Dramatic Improvement</h2><p class='healthcare-content'>55% increase in performance</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=WH&infotype=SA&appname=STGE_TS_ZU_USEN&htmlfid=TSW03287USEN&attachment=TSW03287USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_04.png"
                        },
                        {
                            x: 243,
                            y: 274,
                            content: "<h2 class='healthcare-header'>Outperforms Traditional Storage</h2><p class='healthcare-content'>90% lower latency than high-end disk and 40% lower than SSDs</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=WH&infotype=SA&appname=STGE_TS_ZU_USEN&htmlfid=TSW03287USEN&attachment=TSW03287USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_07.png"
                        }
                    ]
                },
                {
                    title: "Kelsey-Seybold",
                    subtitle: "",
                    description: "The overall goal of Kelsey-Seybold Clinic was to improve patient care. They knew they needed a high-performance, cost-effective storage solution. IBM FlashSystem came to the rescue.",
                    facebookText:"By switching to IBM Flash Storage, Kelsey-Seybold experienced 75% data reduction with 4x lower TCO.",
                    twitterText:"4x lower TCO means more fund for healthcare innovation. #IBM #Flash #KelseySeybold",
                    images: {
                        room: "healthcare/Hospital_Room_03.png"
                    },

                    clickpoints: [
                        {
                            x: 20,
                            y: 235,
                            content: "<h2 class='healthcare-header'>Improved Efficiency</h2><p class='healthcare-content'>One FlashSystem supports 3,700 virtual desktops</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_WA_WC_USEN&htmlfid=WAC12425USEN&attachment=WAC12425USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_01.png"
                        },
                        {
                            x: 138,
                            y: 259,
                            content: "<h2 class='healthcare-header'>Save Space and Cost</h2><p class='healthcare-content'>75% data reduction with 4X lower TCO</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_WA_WC_USEN&htmlfid=WAC12425USEN&attachment=WAC12425USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_09.png"
                        },
                        {
                            x: 278,
                            y: 287,
                            content: "<h2 class='healthcare-header'>Fast and Agile</h2><p class='healthcare-content'>High-performing and responsive environment</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&appname=STGE_WA_WC_USEN&htmlfid=WAC12425USEN&attachment=WAC12425USEN.PDF#loaded",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_02.png"
                        }
                    ]
                },
                {
                    title: "HagaZiekenhuis Hospital",
                    subtitle: "",
                    description: "The teaching hospital, HagaZiekenhuis, needed to improve customer care without changing their applications. Deploying their programs on IBM FlashSystems improved response times - helping doctors access crucial information faster.",
                    facebookText:"When a life is on the line, microseconds make all the difference. HagaZiekenhuis Hospital improved their response times by switching to IBM Flash Storage.",
                    twitterText:"Save microseconds, save lives. #IBM #Flash",
                    images: {
                        room: "healthcare/Hospital_Room_04.png"
                    },

                    clickpoints: [
                        {
                            x: 50,
                            y: 243,
                            content: "<h2 class='healthcare-header'>Trusted and Dependable</h2><p class='healthcare-content'>FlashSystem deployed for 6 critical IT systems</p>",
                            url: "https://www.youtube.com/watch?v=2rBEmrIKiPg",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_13.png"
                        },
                        {
                            x: 170,
                            y: 323,
                            content: "<h2 class='healthcare-header'>More Responsive</h2><p class='healthcare-content'>Improved response times by microseconds</p>",
                            url: "https://www.youtube.com/watch?v=2rBEmrIKiPg",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_10.png"
                        },
                        {
                            x: 325,
                            y: 307,
                            content: "<h2 class='healthcare-header'>Improved Productivity</h2><p class='healthcare-content'>Doctors work more efficiently with more time devoted to patients</p>",
                            url: "https://www.youtube.com/watch?v=2rBEmrIKiPg",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_04.png"
                        }
                    ]
                }

            ]
        },
        {
            path: "financial",
            name: "Financial",
            description: "",
            themeColor: "rgba(253, 184, 19, 1)",
            themeColorMobile: "rgba(253, 184, 19, 0.7)",
            closeColor:"#3b0256",
            learnMoreColor: "#7f1c7d",
            learnMoreText: "#ffffff",


            images: {
                icon: "financial/icon.png",
                nextBuildingIcon: "financial/nextBuildingIcon.png",
                prevBuildingIcon: "financial/prevBuildingIcon.png",
                building: "financial/financial_building.png",
                roof: "financial/financial_roof.png",
                base: "financial/base.png",
                comic: "financial/ComicBookButton.png",
                comicImg: "financial/comicImg.jpg",
                prevIcon: "healthcare/healthcare.png",
                nextIcon: "retail/retail.png",
                closeX: "financial/ClosePurple.png"
            },

            rooms: [
                {
                    title: "Financial Industry",
                    subtitle: "Storage Challenges",
                    description: "The financial industry faces a world where latency is not an option - a world where you have to be quicker than real-time. IBM FlashSystems has helped this industry create narrow timelines and prevent fraud.",
                    facebookText:"",
                    twitterText:"",
                    images: {
                        room: "financial/financial_room_00.png"
                    },

                    clickpoints: [
                        {
                            x: 61,
                            y: 220,
                            content: "<h2 class='financial-header'>Challenge:</h2><p class='financial-content'>The need for data without latency</p><h2 class='financial-header'>Solution:</h2><p class='financial-content-small'>Flash delivers industry-leading latency, in micro-seconds and with up to 50X better performance than enterprise disk systems</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        },
                        {
                            x: 200,
                            y: 248,
                            content: "<h2 class='financial-header'>Challenge:</h2><p class='financial-content'>Narrowing timeframes for batch processing</p><h2 class='financial-header'>Solution:</h2><p class='financial-content-small'>Flash reduces the time that each I/O takes to complete, improving processing time</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        },
                        {
                            x: 272,
                            y: 321,
                            content: "<h2 class='financial-header'>Challenge:</h2><p class='financial-content'>Preventing fraud before it happens</p><h2 class='financial-header'>Solution:</h2><p class='financial-content-small'>Flash enables near real-time fraud detection, which allows suspicious activity to be flagged as it happens</p>",
                            url: "http://www.ibm.com",
                            learnMoreClass: "hide",
                            clickIcon:"modal_icons_blank.png"
                        }
                    ]
                },
                {
                    title: "Wanlian Securities",
                    subtitle: "",
                    description: "Wanlian Securities was growing, and so were their transactions. They needed to minimize latency and improve speed. IBM FlashSystems cut their trade latency while reducing clearing times.",
                    facebookText:"By simply using IBM Flash Storage, Wanlian Securities has 62.5% faster clearing times. Increased speed for increased results.",
                    twitterText:"Wanlian Securities has 62.5% faster times with #IBM #Flash. Increased speed = increased results.",
                    images: {
                        room: "financial/financial_room_01.png"
                    },

                    clickpoints: [
                        {
                            x: 7,
                            y: 205,
                            content: "<h2 class='financial-header'>Faster Acceleration:</h2><p class='financial-content'>100X acceleration of storage I/O to dramatically cut trade latency</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?htmlfid=TSC03356USEN&appname=skmwww",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_01.png"
                        },
                        {
                            x: 170,
                            y: 219,
                            content: "<h2 class='financial-header'>Keep Up:</h2><p class='financial-content'>Move at the speed of the market with clearing times reduced by 62.5%</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?htmlfid=TSC03356USEN&appname=skmwww",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_05.png"
                        },
                        {
                            x: 260,
                            y: 264,
                            content: "<h2 class='financial-header'>Maximize Your Machines:</h2><p class='financial-content'>Faster processing and greater efficiency from 15% increased CPU usage</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?htmlfid=TSC03356USEN&appname=skmwww",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_02.png"
                        }
                    ]
                },
                {
                    title: "Pronto",
                    subtitle: "",
                    description: "Pronto is a credit package company in Uruguay. With so many new products launching, they needed to stay competitive. IBM FlashSystems reduced batch processing times and made deployment of their new products faster than ever.",
                    facebookText:"With IBM Flash Storage, Pronto was able to develop and deploy products faster with reduced overnight batch processing times.",
                    twitterText:"#Pronto knows the way to develop and deploy faster. #IBM #Flash",
                    images: {
                        room: "financial/financial_room_02.png"
                    },

                    clickpoints: [
                        {
                            x: 56,
                            y: 171,
                            content: "<h2 class='financial-header'>Confront Change</h2><p class='financial-content'>Speed, reliability and efficiency to support business growth</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03337USEN&attachment=TSC03337USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_02.png"
                        },
                        {
                            x: 115,
                            y: 257,
                            content: "<h2 class='financial-header'>Better Batches</h2><p class='financial-content'>No more hindering SLAs with reduced overnight batch processing time</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03337USEN&attachment=TSC03337USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_06.png"
                        },
                        {
                            x: 266,
                            y: 245,
                            content: "<h2 class='financial-header'>No More Interruptions</h2><p class='financial-content'>Develop and deploy products faster with uninterrupted service</p>",
                            url: "http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=AB&infotype=PM&htmlfid=TSC03337USEN&attachment=TSC03337USEN.PDF",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_16.png"
                        }
                    ]
                },
                {
                    title: "InfoWorld",
                    subtitle: "",
                    description: "Financial firms today are pushing to decrease the cost of IT infrastructure. With IBM FlashSystem, they experience less power usage, less maintenance and an overall faster performance.",
                    facebookText:"InfoWorld made some massive cuts to their moving parts. With IBM Flash Storage, they were able to eliminate disks to make everything up to 1000x faster.",
                    twitterText:"Nix the moving parts for 1000x faster service. #InfoWorld #IBM #Flash",
                    images: {
                        room: "financial/financial_room_03.png"
                    },

                    clickpoints: [
                        {
                            x: 50,
                            y: 231,
                            content: "<h2 class='financial-header'>Happier Customers</h2><p class='financial-content'>Dynamic customer profiles mean you can provide better customer service</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov22418&S_CMP=web-ibm-st-_-ws-flashresources",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_15.png"
                        },
                        {
                            x: 124,
                            y: 255,
                            content: "<h2 class='financial-header'>Boost Your Bottom Line</h2><p class='financial-content'>Less power usage, less floor space and less maintenance gives you a better</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov22418&S_CMP=web-ibm-st-_-ws-flashresources",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_02.png"
                        },
                        {
                            x: 310,
                            y: 247,
                            content: "<h2 class='financial-header'>Eliminate All The Moving Parts</h2><p class='financial-content'>No moving parts makes everything 100 - 1000x faster</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov22418&S_CMP=web-ibm-st-_-ws-flashresources",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_07.png"
                        }
                    ]
                },
                {
                    title: "FIS client",
                    subtitle: "(online bank)",
                    description: "The world's largest provider of banking technology needed a new way to maintain incredible speed while being deployable across multiple devices. IBM FlashSystem helped them achieve faster times with more than 10,000 transactions.",
                    facebookText:"By implementing IBM Flash Storage, FIS cut crucial times across the whole deployment process – overnight batch processing times, front-end response times and even back up times.",
                    twitterText:"Cut crucial times across your whole process #IBM #Flash #FIS",
                    images: {
                        room: "financial/financial_room_04.png"
                    },

                    clickpoints: [
                        {
                            x: 50,
                            y: 267,
                            content: "<h2 class='financial-header'>No More Late Nights</h2><p class='financial-content'>On average, overnight batch processing time is reduced by 40%</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov26380&S_CMP=web-ibm-st-_-ws-ecosystem",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_06.png"
                        },
                        {
                            x: 148,
                            y: 203,
                            content: "<h2 class='financial-header'>Fast Front-end</h2><p class='financial-content'>Approximately 38% improved front-end application response times</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov26380&S_CMP=web-ibm-st-_-ws-ecosystem",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_10.png"
                        },
                        {
                            x: 264,
                            y: 271,
                            content: "<h2 class='financial-header'>Better Backup</h2><p class='financial-content'>Average backup times reduced by 75%</p>",
                            url: "https://www-01.ibm.com/marketing/iwm/iwm/web/signup.do?source=stg-web&S_PKG=ov26380&S_CMP=web-ibm-st-_-ws-ecosystem",
                            learnMoreClass: "show",
                            clickIcon:"modal_icons_04.png"
                        }
                    ]
                }
            ]
        }


    ]

};
